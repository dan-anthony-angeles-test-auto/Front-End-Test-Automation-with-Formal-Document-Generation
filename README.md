# Front End Test Automation With Formal Document Generation


## About the automation

Front End Test Automation is a Codeless Front End Web application testing tool and automation which enables testers and developers to automate repetitive and numerous tasks on a web application, making your work more productive and effective. Furthermore, it will automatically document the test results of your tests in a Test Results Document provided with the tool.

## To Run It

Simply run the following command: java –jar FrontEndTest.jar FrontEndTestTemplate.xlsx

## More Information

Please download FrontEndTestAutomation_v0.1.pdf for more detailed information.

