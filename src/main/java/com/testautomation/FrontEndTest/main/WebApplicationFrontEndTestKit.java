/*----------------------------------------------------*/
/*	
 	Class name: WebApplicationFrontEndTestKit
 	Description: This class runs the main implementation of the tool
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.main;

import java.util.logging.Logger;

import com.testautomation.FrontEndTest.implementation.FrontEndTestThread;


public class WebApplicationFrontEndTestKit {
	
	public static final Logger LOGGER = Logger.getLogger(WebApplicationFrontEndTestKit.class.getSimpleName());

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FrontEndTestThread frontEnd = new FrontEndTestThread(String.valueOf(args[0]));
		try {
			frontEnd.sequence();
		} catch (Exception e) {
			LOGGER.severe("File cannot be accessed. Check if the file is missing or currently open");
			LOGGER.severe(e.getMessage());
		}
	}

}
