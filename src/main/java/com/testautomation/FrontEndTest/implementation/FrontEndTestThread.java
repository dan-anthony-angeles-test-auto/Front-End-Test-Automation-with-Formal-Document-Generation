/*----------------------------------------------------*/
/*	
 	Class name: FronEndTestThread
 	Description: This class inherits the Basic Thread implementation 
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.implementation;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.testautomation.FrontEndTest.main.WebApplicationFrontEndTestKit;
import com.testautomation.FrontEndTest.util.CommonUtility;
import org.apache.poi.xssf.usermodel.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.testautomation.FrontEndTest.fields.CommonFields;
import com.testautomation.FrontEndTest.fields.ExcelFields;
import com.testautomation.FrontEndTest.fields.SeleniumFields;
import com.testautomation.FrontEndTest.thread.BasicThread;
import com.testautomation.FrontEndTest.util.ExcelUtility;
import com.testautomation.FrontEndTest.util.SeleniumUtility;

public class FrontEndTestThread extends BasicThread{
	
	private static final Logger LOGGER = WebApplicationFrontEndTestKit.LOGGER;
	
	private String warningText = "";
	private int numOfTests = 0;
	private int numOfPassed = 0;
	private int numOfFailed = 0;
	private int numOfRows = 0;
	private int numOfCol = 0;
	private int configRows = 5;
	private int debugLevel = 1;
	private String testPlan = "Test Plan";
	private String testSumm = "Tests Results Summary";
	private String config = "Configuration Sheet";
	private List <String> testSummaryRow = new ArrayList<>();
	private List <List<String>> testSummary = new ArrayList<>();
	private List <String> testSuiteList = new ArrayList<>();
	private PrintWriter logWrite = null;
	private FileInputStream fIP = null;
	private XSSFWorkbook wb = null;
	private XSSFSheet testPlanSheet = null;
	private XSSFSheet configSheet = null;
	private XSSFSheet testSummarySheet = null;
	private XSSFSheet testSuiteSheet = null;
	private WebDriver webDriver = null;
	
	//Class declaration
	ExcelFields excelFields = null;
	CommonFields commonFields = null;
	SeleniumFields seleniumFields = null;
	CommonUtility commonUtil = new CommonUtility();
	ExcelUtility excelUtil = new ExcelUtility();
	SeleniumUtility seleniumUtil = new SeleniumUtility();
	
	public FrontEndTestThread(String excelFilePath) {
		excelFields = new ExcelFields(excelFilePath);
	}
	
	@Override
	public void initialize() throws Exception {
		String logFileName = "";
		String webDriver = "";
		boolean addTestSuiteToList = false;
		List <String> missingSheets = new ArrayList<>();
		StringBuilder exceptionMessage = new StringBuilder();
		StringBuilder information = new StringBuilder();
		String value = "";
		boolean logFileCreation = true;
		
		try {
			
			if (commonUtil.fileExists(excelFields.getExcelFile())) { //check if file exists
				fIP = excelUtil.setFiletoInputStream(excelFields.getExcelFile());
				wb = excelUtil.openWorkbook(fIP);
				
				configSheet = excelUtil.selectSheet(this.wb, this.config);
				
				if (excelUtil.getCellValue(configSheet, 1, 1) != "") {
					LOGGER.info("Debug Level:" + excelUtil.getCellValue(configSheet, 1, 1));
					debugLevel = (int)Double.parseDouble(excelUtil.getCellValue(configSheet, 1, 1));
				}
				
				LOGGER.info("Generate Log File: " + excelUtil.getCellValue(configSheet, 3, 1));
				
				if (excelUtil.getCellValue(configSheet, 3, 1).contains("Enabled")) {
					logFileCreation = true;
				} else {
					logFileCreation = false;
				}
					
				commonFields = new CommonFields(debugLevel, 
						logFileCreation,
						excelUtil.getCellValue(configSheet, 4, 1),
						excelUtil.getCellValue(configSheet, 5, 1));
				
				if (commonUtil.fileExists(excelUtil.getCellValue(configSheet, 2, 1))) {
					webDriver = excelUtil.getCellValue(configSheet, 2, 1);
				} else {
					throw new Exception("WebDriver is not found: " + excelUtil.getCellValue(configSheet, 2, 1));
				}

				if (commonFields.getLogFileCreation()) { // if log file creation is true

					if (commonUtil.fileExists(commonFields.getLogFilePath())) {
					
						if (commonFields.getLogFileName() == "") { // if there is no specified log file name
							logFileName = excelUtil.getWorkBookName(commonFields.getLogFilePath());
						} else {
							logFileName = commonFields.getLogFileName();
						}

						
						logWrite = commonUtil.logFileCreation(LOGGER, commonFields.getLogFilePath(), 
								logFileName, commonFields.getDebugMsgLvl());
	                	logWrite.flush();
	                	logWrite.close();
					} else {
						throw new Exception("Log File Path does not exist: " + excelUtil.getCellValue(configSheet, 4, 1)); 
					}
				}
				
				testPlanSheet = excelUtil.selectSheet(this.wb, this.testPlan);
				numOfRows = excelUtil.numberOfRows(testPlanSheet);
				
				if (commonFields.getDebugMsgLvl() > 0) {
					LOGGER.info("Retrieving Test Suite Names");
				}
				if (commonFields.getDebugMsgLvl() > 2) {
					LOGGER.info("Test Suite Names are the following: ");
				}

				for (int i = 0; i < numOfRows; i++) {

					
					if (excelUtil.getCellValue(testPlanSheet, i, 1).contains("Web URL:")) { //get URL and web driver common executable
						seleniumFields = new SeleniumFields(webDriver, excelUtil.getCellValue(testPlanSheet, i, 2));
						LOGGER.info("Web URL: " + excelUtil.getCellValue(testPlanSheet, i, 2));
					}
					
					if (excelUtil.getCellValue(testPlanSheet, i, 2).contains("Test Suite Name")) {
						addTestSuiteToList = true;
					}
					
					if (addTestSuiteToList) {		
						if (excelUtil.getCellValue(testPlanSheet, i, 1).contains("Execute")) { //add to Test Suite List
							if (!excelUtil.getCellValue(testPlanSheet, i, 1).contains("Don't Execute")) { //add to Test Suite List
								testSuiteList.add(excelUtil.getCellValue(testPlanSheet, i, 2));
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info(excelUtil.getCellValue(testPlanSheet, i, 2));
								}
							}
						}
					}
				}
				
				addTestSuiteToList = false;
				missingSheets = excelUtil.sheetExists(wb, testSuiteList); //check if sheets exists in the workbook
				if (missingSheets.size() > 0) {
					exceptionMessage.append("The following sheets are not found in this workbook: %n");
					for (int i = 0; i < missingSheets.size(); i++) {
						exceptionMessage.append(missingSheets.get(i) + "%n");
					}
					exceptionMessage.append("Please indicate the correct sheet names to be used in this application");
					throw new Exception(exceptionMessage.toString());
				} else {
					if (commonFields.getDebugMsgLvl() > 1) {
						information.append("The following test suites will be executed: " + "\n");
						for (int i = 0; i < testSuiteList.size(); i++) {
							information.append(testSuiteList.get(i) + "\n");
						}
						LOGGER.info(information.toString());
					}
					if (commonFields.getDebugMsgLvl() > 0) {
						LOGGER.info("Test Suite Names Retrieved");
					}
				}
			} else { 
				LOGGER.severe("Excel File does not exist: " + excelFields.getExcelFile().replace("\\\\","\\"));
				
			}
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	@Override
	public void setup() throws Exception {
		try {// set the web driver to be used for execution
			if (commonFields.getDebugMsgLvl() > 1) {
				LOGGER.info("Set Web Driver: " + seleniumFields.getWebDriverFile());
			}
			webDriver = seleniumUtil.setWebDriver(seleniumFields.getWebDriverFile());
			
		} catch (Exception e){
			LOGGER.severe(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	@Override
	public void execute() throws Exception {
		//these are the columns indicated in the Test Suite Sheet
		int testCaseName = 1;
		int actualTestName = 2;
		int execute = 0;
		int fullXp = 3;
		int elementTypeNum = 4;
		int operation = 5;
		int insertVal = 6;
		int waitCond = 7;
		int waitInSec = 8;
		int textToWait = 9;
		int expectedVal = 10;
		int actVal = 11;
		int passFail = 12;
		int runResult = 13;
		int runLog = 14;
		int seconds = 10;
		boolean runTest = false;
		boolean executeTest = false;
		boolean elementCondition = false;
		boolean endBrowser = false;
		boolean pass = true;
		boolean addToTestSummary = false;
		int rows = 0;
		String copyFromAWebElement = "";
		String getTestName = "";
		String operationName = "";
		String testDescription = "";
		String waitCondition = "";
		String elementType = "";
		String fullXpath = "";
		String insertValues = "";
		String textToWaitFor = "";
		String expectedValues = "";
		String testCaseNum = "";
		WebElement webElement = null;
		WebDriverWait wait = null;

		
		try {
			
			for (int testSuite = 0; testSuite < testSuiteList.size(); testSuite++) { //read through the list of test suites
				runTest = false;
				executeTest = false;

				
				if (commonFields.getDebugMsgLvl() > 0) {
					LOGGER.info("Test Suite to Execute: " + testSuiteList.get(testSuite));
				}
				
				testSuiteSheet = excelUtil.selectSheet(wb, testSuiteList.get(testSuite));
				numOfRows = excelUtil.numberOfRows(testSuiteSheet);

				if (commonFields.getDebugMsgLvl() > 2) {
					LOGGER.info("Number of Rows in " + testSuiteList.get(testSuite) + " is " + numOfRows);
				}
				
				if (commonFields.getDebugMsgLvl() > 1) {
					LOGGER.info("Test Start:");
				}
				
				for (int rowsToRead = 0; rowsToRead < numOfRows; rowsToRead++) { //parse the test suite sheet row by row
					
					if (excelUtil.getCellValue(testSuiteSheet, rowsToRead, testCaseName).contains("Release Version")) {
						LOGGER.info("Release Version: " + excelUtil.getCellValue(testSuiteSheet, rowsToRead, actualTestName));
					}
					
					if (excelUtil.getCellValue(testSuiteSheet, rowsToRead, testCaseName).contains("Version Description")) {
						LOGGER.info("Version Description: " + excelUtil.getCellValue(testSuiteSheet, rowsToRead, actualTestName));
					}
						
					if (excelUtil.getCellValue(testSuiteSheet, rowsToRead, testCaseName).contains("Test Case Name")) { //check on the test case first
						pass = true;
						runTest = true;
						executeTest = false;
						getTestName = excelUtil.getCellValue(testSuiteSheet, rowsToRead, actualTestName);
						if (commonFields.getDebugMsgLvl() > 1) {
							LOGGER.info("Test Case: " + getTestName);
						}
					}
					
					if (excelUtil.getCellValue(testSuiteSheet, rowsToRead, testCaseName).contains("Test Description")) { //get the test case number
						testCaseNum = excelUtil.getCellValue(testSuiteSheet, rowsToRead, execute);
					}
					
					if (runTest) { //by seeing that we are now in the test cases rows, we will now run the tests
						if (executeTest) {
							rows = rowsToRead;
							operationName = excelUtil.getCellValue(testSuiteSheet, rowsToRead, operation);
							testDescription = excelUtil.getCellValue(testSuiteSheet, rowsToRead, actualTestName);
							waitCondition = excelUtil.getCellValue(testSuiteSheet, rowsToRead, waitCond);
							fullXpath = excelUtil.getCellValue(testSuiteSheet, rowsToRead, fullXp);
							insertValues = excelUtil.getCellValue(testSuiteSheet, rowsToRead, insertVal);
							textToWaitFor = excelUtil.getCellValue(testSuiteSheet, rowsToRead, textToWait);
							expectedValues = excelUtil.getCellValue(testSuiteSheet, rowsToRead, expectedVal);
							elementType = excelUtil.getCellValue(testSuiteSheet, rowsToRead, elementTypeNum);
							excelUtil.setCellValue(testSuiteSheet, rowsToRead, actVal, "");  //set results to blank
							excelUtil.setCellValue(testSuiteSheet, rowsToRead, passFail, ""); //set results to blank
							excelUtil.setCellValue(testSuiteSheet, rowsToRead, runResult, ""); //set results to blank
							excelUtil.setCellValue(testSuiteSheet, rowsToRead, runLog, ""); //set results to blank
							
							if (excelUtil.getCellValue(testSuiteSheet, rowsToRead, waitInSec) != "") {
								seconds = (int)Double.parseDouble(excelUtil.getCellValue(testSuiteSheet, rowsToRead, waitInSec));
							}

							
							//Asynchronous operation Start
							//perform thread sleep if it is being called.
							if (operationName.contains("Test Sleep")) {
								if (seconds == 0) {
									seconds = 10;
								}
								
								seleniumUtil.testSleep(seconds);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
									LOGGER.info("Sleep Duration in seconds: " + String.valueOf(seconds));
								}
							}

							if (operationName.contains("Open Browser")) {
								seleniumUtil.openBrowser(webDriver, seleniumFields.getUrl());
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
				
							if (operationName.contains("Close Browser")) {
								seleniumUtil.closeBrowser(webDriver,seleniumFields.getWebDriverFile());
								endBrowser = false;
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							
							if (operationName.contains("Refresh")) {
								seleniumUtil.refreshPage(webDriver);
								endBrowser = false;
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}

							if (operationName.contains("FullScreen")) {
								seleniumUtil.fullScreen(webDriver);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							if (operationName.contains("Maximize Screen")) {
								seleniumUtil.maximizeScreen(webDriver);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							
							if (operationName.contains("Minimize Screen")) {
								seleniumUtil.minimizeScreen(webDriver);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							
							if (operationName.contains("Scroll Up to top page")) {
								seleniumUtil.scrollUpToTopPage(webDriver);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							
							if (operationName.contains("Scroll Down to bottom page")) {
								seleniumUtil.scrollDownToBottomPage(webDriver);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							
							if (operationName.contains("Switch to parent frame")) {
								seleniumUtil.switchToParentFrame(webDriver);;
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							//Asynchronous operation End
							
							//Wait Conditions Start
							if (waitCondition == "") {
								seconds = 10;
								if (fullXpath != "") {
									wait = seleniumUtil.explicitWait(webDriver, seconds);
									webElement = seleniumUtil.elementToBeClickable(wait, fullXpath, elementType);								
									if (commonFields.getDebugMsgLvl() > 2) {
										LOGGER.info("Waiting for the Element: " + fullXpath);
										LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
									}
								}
							} else {
								if (!waitCondition.contains("No Wait Condition")) {

									LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
									if (seconds == 0) {
										seconds = 10;
									}
									wait = seleniumUtil.explicitWait(webDriver, seconds);
									
									if (waitCondition.contains("Frame to be available")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the Frame to Switch to: " + fullXpath);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										webDriver = seleniumUtil.frameToBeAvailableAndSwitchToIt(wait, fullXpath, elementType);								
									}
									
									if (waitCondition.contains("Element To Be Clickable")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the Element: " + fullXpath);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										webElement = seleniumUtil.elementToBeClickable(wait, fullXpath, elementType);								
									}
									
									if (waitCondition.contains("Element To Be Selected")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the Element: " + fullXpath);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										webElement = seleniumUtil.elementToBeSelected(webDriver, wait, fullXpath, elementType);								
									}
									
									if (waitCondition.contains("Invisibility Of The Element Located")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the Element to be invisible: " + fullXpath);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										elementCondition = seleniumUtil.invisibilityOfTheElementLocated(webDriver, wait, fullXpath, elementType);
										if (!elementCondition) {
											LOGGER.severe("Element with this XPath did not become invisible: " + fullXpath);
										}
									}
	
									if (waitCondition.contains("Invisibility Of Element With Text")) {
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the Element to be invisible: " + fullXpath);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										elementCondition = seleniumUtil.invisibilityOfElementWithText(webDriver, wait, fullXpath, elementType, textToWaitFor);
										if (!elementCondition) {
											LOGGER.info("Text can still be found: " + textToWaitFor);
											LOGGER.severe("Element with this XPath did not become invisible: " + fullXpath);
										}
									}
									
									if (waitCondition.contains("Presence Of Element Located")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the presence of the element: " + fullXpath);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										webElement = seleniumUtil.presenceOfElementLocated(wait, fullXpath, elementType);
									}
									
									if (waitCondition.contains("Text To Be Present In Element Located")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the presence of the element: " + fullXpath);
											LOGGER.info("Text to Wait For: " + textToWaitFor);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										webElement = seleniumUtil.textToBePresentInElementLocated(webDriver, wait, fullXpath, elementType, textToWaitFor);
									}
									
									if (waitCondition.contains("Title Is")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the presence of the element: " + fullXpath);
											LOGGER.info("Title to Wait For: " + textToWaitFor);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										elementCondition = seleniumUtil.titleIs(wait, textToWaitFor);
										if (!elementCondition) {
											LOGGER.info("Title not found: " + textToWaitFor);
											LOGGER.severe("Element with this XPath can not be found: " + fullXpath);
										} else {
											webElement = seleniumUtil.findElement(webDriver, fullXpath, elementType);
										}
									}
	
									if (waitCondition.contains("Title Contains")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the presence of the element: " + fullXpath);
											LOGGER.info("Title to Wait For: " + textToWaitFor);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										elementCondition = seleniumUtil.titleContains(wait, textToWaitFor);
										if (!elementCondition) {
											LOGGER.info("Title not found: " + textToWaitFor);
											LOGGER.severe("Element with this XPath can not be found: " + fullXpath);
										} else {
											webElement = seleniumUtil.findElement(webDriver, fullXpath, elementType);
										}
									}
									
									if (waitCondition.contains("Visibility Of Element Located")) { 
										if (commonFields.getDebugMsgLvl() > 2) {
											LOGGER.info("Waiting for the visibility of the element " + fullXpath);
											LOGGER.info("Wait in seconds: " + String.valueOf(seconds));
										}
										webElement = seleniumUtil.visibilityOfElementLocated(wait, fullXpath, elementType);
									}
								}
							}
							//wait Conditions End
							//After wait conditions have been satisfied for the element to appear we can now perform actions with it.
							
							if (operationName.contains("Find Element")) {
								seleniumUtil.findElement(webDriver, fullXpath, elementType);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							
							if (operationName.contains("Click")) {
								seleniumUtil.click(webElement);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							
							if (operationName.contains("Enter Key")) {
								seleniumUtil.enterKey(webElement);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}

							if (operationName.contains("Clear Values")) {
								seleniumUtil.clearValues(webElement);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
								}
							}
							
							if (operationName.contains("Input Values")) {
								seleniumUtil.inputValues(webElement, insertValues);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
									LOGGER.info("Input Value: " + insertValues);
								}
							}

							if (operationName.contains("Copies a value from a web element")) {
								copyFromAWebElement = seleniumUtil.getValuesFromAWebElement(webElement);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
									LOGGER.info("Value Retrieved: " + copyFromAWebElement);
								}
							}
							
							if (operationName.contains("Paste Values to a Web Element")) {
								seleniumUtil.inputValues(webElement, copyFromAWebElement);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
									LOGGER.info("Input Value: " + copyFromAWebElement);
								}
							}
							
							if (operationName.contains("Compare values")) {
								copyFromAWebElement = seleniumUtil.getValuesFromAWebElement(webElement);
								if (commonFields.getDebugMsgLvl() > 2) {
									LOGGER.info("Test Description: " + testDescription);
									LOGGER.info("Operation: " + operationName);
									LOGGER.info("Expected Value: " + expectedValues);
									LOGGER.info("Actual Value: " + copyFromAWebElement);
								}
								excelUtil.setCellValue(testSuiteSheet, rowsToRead, actVal, copyFromAWebElement);
								
								if (expectedValues.compareTo(copyFromAWebElement) == 0) {
									excelUtil.setCellValue(testSuiteSheet, rowsToRead, passFail, "PASS");
								} else {
									excelUtil.setCellValue(testSuiteSheet, rowsToRead, passFail, "FAIL");
									pass = false;
								}
							}
														
							// if no issues found during this step we can mark it as successful
							excelUtil.setCellValue(testSuiteSheet, rows, runResult, "Successful");
							//Operations End
							if (excelUtil.getCellValue(testSuiteSheet, rowsToRead, execute).contains("<END>")) {// For Test Suite Summary
								runTest = false;
								executeTest = false;
								testSummaryRow = new ArrayList<>();
								testSummaryRow.add(testSuiteList.get(testSuite));
								testSummaryRow.add(testCaseNum);
								testSummaryRow.add(getTestName);
								if (pass) {
									numOfPassed += 1;
									testSummaryRow.add("PASS");
								} else {
									numOfFailed += 1;
									testSummaryRow.add("FAIL");
								}
								testSummary.add(testSummaryRow);
								numOfTests += 1;
							}
						} //if execute test
						if (excelUtil.getCellValue(testSuiteSheet, rowsToRead, execute).contains("Execute")) {
							executeTest = true;
						}
						if (excelUtil.getCellValue(testSuiteSheet, rowsToRead, execute).contains("Don't Execute")) {
							executeTest = false;
						}						
						
					}//if run test
				}
			}
			if (commonFields.getDebugMsgLvl() > 1) {
				LOGGER.info("Test End");
			}
			
		} catch (Exception e) {
			endBrowser = true;
			numOfFailed += 1;
			excelUtil.setCellValue(testSuiteSheet, rows, runResult, "Unsuccessful");
			excelUtil.setCellValue(testSuiteSheet, rows, runLog, e.getMessage());
			LOGGER.severe(e.getMessage());
			throw new Exception(e.getCause());
		} finally {
			if (endBrowser == true) {
				seleniumUtil.closeBrowser(webDriver,seleniumFields.getWebDriverFile());
				excelUtil.closeInputFileStream(fIP);
				excelUtil.closeWorkbook(wb, excelFields.getExcelFile());
			}
		}
	}
	
	@Override
	public void exit() throws Exception {
		
		if (commonFields.getDebugMsgLvl() > 0) {
			LOGGER.info("Test Summary");
		}
		
		String warnMessage = "Warning: Do not change the tab name for the Test Results Summary. Changing the name will result to Automation Failure";
		String numOfTestRun = "No. of Tests Run";
		String testPassed = "Test Passed";
		String testFailed = "Test Failed";
		String testSuiteName = "Test Suite Name";
		String testCaseNum = "Test Case No.";
		String testCaseName = "Test Case Name";
		String result = "Result";
		String val = "";
		StringBuilder testLogSummary = new StringBuilder();
		StringBuilder passedTest = new StringBuilder();
		StringBuilder failedTest = new StringBuilder();
		int addRow = 5;

		testSummarySheet = excelUtil.selectSheet(wb, testSumm);

	    excelUtil.clearSheet(testSummarySheet); //clear previous results first
		excelUtil.setCellValue(testSummarySheet, 0, 0, warnMessage);
		excelUtil.setCellValue(testSummarySheet, 1, 0, numOfTestRun);
		excelUtil.setCellValue(testSummarySheet, 1, 1, String.valueOf(numOfTests));
		excelUtil.setCellValue(testSummarySheet, 2, 0, testPassed);
		excelUtil.setCellValue(testSummarySheet, 2, 1, String.valueOf(numOfPassed));
		excelUtil.setCellValue(testSummarySheet, 3, 0, testFailed);
		excelUtil.setCellValue(testSummarySheet, 3, 1, String.valueOf(numOfFailed));
		excelUtil.setCellValue(testSummarySheet, 4, 0, testSuiteName);
		excelUtil.setCellValue(testSummarySheet, 4, 1, testCaseNum);
		excelUtil.setCellValue(testSummarySheet, 4, 2, testCaseName);
		excelUtil.setCellValue(testSummarySheet, 4, 3, result);
		
		testLogSummary.append("Total No. of Tests: " + String.valueOf(numOfTests) + "\n");
		testLogSummary.append("Total No. of Passed Tests: " + String.valueOf(numOfPassed) + "\n");
		testLogSummary.append("Total No. of Failed Tests: " + String.valueOf(numOfFailed) + "\n");
		
		passedTest.append("Passed Test:" + "\n");
		failedTest.append("Failed Test:" + "\n");
		for (int tS = 0; tS < testSummary.size(); tS++) {
			for (int testDetails = 0; testDetails < testSummary.get(tS).size(); testDetails ++) {
				val = testSummary.get(tS).get(testDetails);
				if  (testDetails == 0) {
					excelUtil.setCellValue(testSummarySheet, addRow, 0, val);
				} else if (testDetails == 1) {
					excelUtil.setCellValue(testSummarySheet, addRow, 1, val);
				} else if (testDetails == 2) {
					excelUtil.setCellValue(testSummarySheet, addRow, 2, val);
					testCaseName = val;
				} else if (testDetails == 3) {
					excelUtil.setCellValue(testSummarySheet, addRow, 3, val);
					result = val;
				}
			}
			if (result == "PASS") {
				passedTest.append(testCaseName + "\n");		
			} else {
				failedTest.append(testCaseName + "\n");
			}
			addRow += 1;

		}
		testLogSummary.append("\n");
		testLogSummary.append(passedTest.toString() + "\n");
		testLogSummary.append(failedTest.toString() + "\n");
		
		if (commonFields.getDebugMsgLvl() > 0) {
			LOGGER.info(testLogSummary.toString());
		}		
		excelUtil.closeInputFileStream(fIP);
		excelUtil.closeWorkbook(wb, excelFields.getExcelFile());
		if (commonFields.getDebugMsgLvl() > 0) {
			LOGGER.info("Test Execution Finished");
		}
	}
}
