/*----------------------------------------------------*/
/*	
 	Class name: SeleniumUtility
 	Description: This class contains utilities for Selenium Related Operations
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.util;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import com.testautomation.FrontEndTest.main.WebApplicationFrontEndTestKit;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumUtility {

	private static final Logger LOGGER = WebApplicationFrontEndTestKit.LOGGER;
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: explicitWait
	 	Description: sets wait time for the element to appear
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
	  			seconds - time before timeout appears
		Output Parameters:
		 		WebDriverWait - the amount of wait time for the DOM to load before timing out 
	*/
	/*----------------------------------------------------*/
	public WebDriverWait explicitWait(WebDriver webDriver, int seconds) throws Exception {
		WebDriverWait webDriverWait = null;
		webDriverWait = new WebDriverWait(webDriver, seconds);
		return webDriverWait;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: setWebDriver
	 	Description: sets the webdriver depending on the browser to be used
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriverPath - the path where the web driver is located
		Output Parameters:
				webDriver - the webDriver that controls the browser
	*/
	/*----------------------------------------------------*/
	public WebDriver setWebDriver(String webDriverPath) throws Exception {
		WebDriver driver = null;
        if (webDriverPath.contains("chrome")) {
        	System.setProperty("webdriver.chrome.driver", webDriverPath);
        	driver = new ChromeDriver();
        } else if (webDriverPath.contains("gecko")) {
        	System.setProperty("webdriver.gecko.driver", webDriverPath);
        	driver = new FirefoxDriver();        	
        } else if (webDriverPath.contains("ie")) {
        	System.setProperty("webdriver.ie.driver", webDriverPath);
        	driver = new InternetExplorerDriver();        	
        } else if (webDriverPath.contains("edge")) {
        	System.setProperty("webdriver.edge.driver", webDriverPath);
        	driver = new EdgeDriver();
        } else {
        	throw new Exception("Invalid WebDriver: " + webDriverPath);
        }
        return driver;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: locateElement
	 	Description: ways to locate an element
	 	Modifier: Public Method
	  	Input Parameters: 
	  			fullXPath - path or any identification of the element
	  			type - parsing of the path. By default this is an xpath
		Output Parameters:
				By locator - depending on what element locator was used
	*/
	/*----------------------------------------------------*/
	public By locateElement(String fullXPath, String type) throws Exception{
		
		By locator = null;
		
		if (type.contains("ID")) {
			By.id(fullXPath);
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				By.partialLinkText(fullXPath);
			} else {
				By.linkText(fullXPath);	
			}
		} else if (type.contains("Name")) {
			By.name(fullXPath);
		} else if (type.contains("Class Name")) {
			By.className(fullXPath);
		} else if (type.contains("Tag Name")) {
			By.tagName(fullXPath);
		} else if (type.contains("Xpath")) {
			By.xpath(fullXPath);
		} else {
			By.xpath(fullXPath);
		}
		return locator;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: openBrowser
	 	Description: opens the broswer and accesses the website through the URL
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
	  			url - the string that contains the website path
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	
	public void openBrowser(WebDriver webDriver, String url) throws Exception {
		webDriver.get(url);
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: closeBrowser
	 	Description: closes the browser.
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
	  			webDriverPath - the path where the web driver is located. This is used to determine the type driver used
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void closeBrowser(WebDriver webDriver,String webDriverPath) throws Exception {
		
		webDriver.close();
		
        if (webDriverPath.contains("chrome")) {
        	Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
        } else if (webDriverPath.contains("gecko")) {
        	Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");  	
        } else if (webDriverPath.contains("ie")) {
        	Runtime.getRuntime().exec("taskkill /F /IM iedriver.exe");
        } else if (webDriverPath.contains("edge")) {
        	Runtime.getRuntime().exec("taskkill /F /IM edgedriver.exe");
        } else {
        	throw new Exception("Invalid WebDriver: " + webDriverPath);
        }
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: refreshPage
	 	Description: refreshes the page with current URL
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void refreshPage(WebDriver webDriver) throws Exception {
		webDriver.navigate().refresh();
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: findElement
	 	Description: finds the web element
	 	Modifier: Public Method
	  	Input Parameters: 
	  		  	webDriver - the webDriver that controls the browser 
				Xpath - path of the element
				type - type of the path
		Output Parameters:
				webElement - the element to be used in this method
	*/
	/*----------------------------------------------------*/
	public WebElement findElement(WebDriver webdriver, String xpath, String type) throws Exception {
		WebElement element = null;
		if (type.contains("ID")) {
			element = webdriver.findElement(By.id(xpath));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				element = webdriver.findElement(By.partialLinkText(xpath));
			} else {	
				element = webdriver.findElement(By.linkText(xpath));
			}
		} else if (type.contains("Name")) {
			element = webdriver.findElement(By.name(xpath));
		} else if (type.contains("Class Name")) {
			element = webdriver.findElement(By.className(xpath));
		} else if (type.contains("Tag Name")) {
			element = webdriver.findElement(By.tagName(xpath));
		} else if (type.contains("Xpath")) {
			element = webdriver.findElement(By.xpath(xpath));
		} else {
			element = webdriver.findElement(By.xpath(xpath));
		}
		
		return element;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: click
	 	Description: clicks the web element
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webElement - the element to be used in this method
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void click(WebElement webelement) throws Exception {
		webelement.click();
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: clearValues
	 	Description: clears the values in the web element
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webElement - the element to be used in this method
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void clearValues(WebElement webelement) throws Exception {
		webelement.clear();
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: inputValues
	 	Description: inputs values in the web element
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webElement - the element to be used in this method
	  			text - the value to be input to the web element
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void inputValues(WebElement webelement, String text) throws Exception {
		webelement.sendKeys(text);
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: enterKey
	 	Description: simulates an enter key
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webElement - the element to be used in this method
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void enterKey(WebElement webelement) throws Exception {
		webelement.sendKeys(Keys.RETURN);
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: scrollUpToTopPage
	 	Description: scrolls the browser to top page
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void scrollUpToTopPage(WebDriver webdriver) throws Exception {
        JavascriptExecutor js = (JavascriptExecutor) webdriver;
        js.executeScript("window.scrollTo(0, 0");		
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: scrollDownToBottomPage
	 	Description: scrolls the browser to bottom page
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void scrollDownToBottomPage(WebDriver webdriver) throws Exception {
        JavascriptExecutor js = (JavascriptExecutor) webdriver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: scrollIntoView
	 	Description: scrolls the browser to where the web element is located
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
	  			webElement - the element to be used in this method
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void scrollIntoView(WebDriver webdriver, WebElement element) throws Exception {
        JavascriptExecutor js = (JavascriptExecutor) webdriver;
        js.executeScript("arguments[0].scrollIntoView();",element );
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: testSleep
	 	Description: suspends the execution to give way for processing time
	 	Modifier: Public Method
	  	Input Parameters: 
	  			seconds - no of seconds the execution is suspended
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void testSleep(int seconds) throws Exception {
		TimeUnit.MILLISECONDS.sleep(seconds * 1000);
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: getValuesFromAWebElement
	 	Description: gets the value from a web element and can be used as input to another
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webElement - the element to be used in this method
		Output Parameters:
				String -  value obtained in this method
	*/
	/*----------------------------------------------------*/
	public String getValuesFromAWebElement(WebElement webelement) throws Exception {
		return webelement.getText();
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: maximizeScreen
	 	Description: maximizes the browser
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void maximizeScreen(WebDriver webdriver) throws Exception {
		webdriver.manage().window().maximize();
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: minimizeScreen
	 	Description: minimizes the browser
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void minimizeScreen(WebDriver webdriver) throws Exception {
		Point p = new Point(0,3000);
		webdriver.manage().window().setPosition(p);
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: fullScreen
	 	Description: sets the browser to full screen
	 	Modifier: Public Method
	  	Input Parameters: 
	  			webDriver - the webDriver that controls the browser
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/
	public void fullScreen(WebDriver webdriver) throws Exception {
		webdriver.manage().window().fullscreen();
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: elementToBeClickable
	 	Description: waits for the element to be clickable
	 	Modifier: Public Method
	  	Input Parameters: 
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				xpath - path of the element
				type - type of the path
		Output Parameters:
				webElement - the element to be used in this method
	*/
	/*----------------------------------------------------*/
	public WebElement elementToBeClickable(WebDriverWait wait, String xpath, String type) throws Exception {
		WebElement element = null;
		if (type.contains("ID")) {
			element = wait.until(ExpectedConditions.elementToBeClickable(By.id(xpath)));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				element = wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(xpath)));
			} else {	
				element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText(xpath)));
			}
		} else if (type.contains("Name")) {
			element = wait.until(ExpectedConditions.elementToBeClickable(By.name(xpath)));
		} else if (type.contains("Class Name")) {
			element = wait.until(ExpectedConditions.elementToBeClickable(By.className(xpath)));
		} else if (type.contains("Tag Name")) {
			element = wait.until(ExpectedConditions.elementToBeClickable(By.tagName(xpath)));
		} else if (type.contains("CSS Selector")) {
			element = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(xpath)));	
		} else if (type.contains("Xpath")) {
			element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		} else {
			element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		}
		 
		return element;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: elementToBeSelected
	 	Description: waits for the element to be clickable
	 	Modifier: Public Method
	  	Input Parameters:
	  		  	webDriver - the webDriver that controls the browser 
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				xpath - path of the element
				type - type of the path
		Output Parameters:
				webElement - the element to be used in this method
	*/
	/*----------------------------------------------------*/
	public WebElement elementToBeSelected(WebDriver driver, WebDriverWait wait, String xpath, String type) throws Exception {
		Boolean elementToAppear = false;
		WebElement element = null;
		if (type.contains("ID")) {
			elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.id(xpath)));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.partialLinkText(xpath)));
			} else {	
				elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.linkText(xpath)));
			}
		} else if (type.contains("Name")) {
			elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.name(xpath)));
		} else if (type.contains("Class Name")) {
			elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.className(xpath)));
		} else if (type.contains("Tag Name")) {
			elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.tagName(xpath)));
		} else if (type.contains("CSS Selector")) {
			elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.cssSelector(xpath)));			
		} else if (type.contains("Xpath")) {
			elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.xpath(xpath)));
		} else {
			elementToAppear = wait.until(ExpectedConditions.elementToBeSelected(By.xpath(xpath)));
		}
		 
		if (elementToAppear) {
			element = findElement(driver,xpath,type);
		} else {
			throw new Exception("WebElement to be selected is not found. Xpath: " + xpath);
		}
		return element;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: invisibilityOfTheElementLocated
	 	Description: An expectation for checking that an element is either invisible or not present on the DOM.
	 	Modifier: Public Method
	  	Input Parameters:
	  		  	webDriver - the webDriver that controls the browser 
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				xpath - path of the element
				type - type of the path
		Output Parameters:
				boolean - returns true if the element has disappeared
	*/
	/*----------------------------------------------------*/
	public boolean invisibilityOfTheElementLocated(WebDriver driver, WebDriverWait wait, String xpath, String type) throws Exception {
		Boolean elementToDisappear = false;		
		WebElement element = null;
		
		if (type.contains("ID")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(xpath)));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.partialLinkText(xpath)));
			} else {	
				elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText(xpath)));
			}
		} else if (type.contains("Name")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.name(xpath)));
		} else if (type.contains("Class Name")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(xpath)));
		} else if (type.contains("Tag Name")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.tagName(xpath)));
		} else if (type.contains("CSS Selector")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(xpath)));
		} else if (type.contains("Xpath")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
		} else {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
		}
		 
		if (elementToDisappear) {
			element = findElement(driver,xpath,type);
		} else {
			throw new Exception("WebElement to be selected is not found. Xpath: " + xpath);
		}
		
		return elementToDisappear;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: invisibilityOfElementWithText
	 	Description: An expectation for checking that an element with text is either invisible or not present on the DOM.
	 	Modifier: Public Method
	  	Input Parameters:
	  		  	webDriver - the webDriver that controls the browser 
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				xpath - path of the element
				type - type of the path
				text - the text to be evaluated
		Output Parameters:
				boolean - returns true if the element has disappeared
	*/
	/*----------------------------------------------------*/
	public boolean invisibilityOfElementWithText(WebDriver driver, WebDriverWait wait, String xpath, String type, String text) throws Exception {		
		Boolean elementToDisappear = false;		
		WebElement element = null;
		
		if (type.contains("ID")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.id(xpath),text));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.partialLinkText(xpath),text));
			} else {	
				elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.linkText(xpath),text));
			}
		} else if (type.contains("Name")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.name(xpath),text));
		} else if (type.contains("Class Name")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.className(xpath),text));
		} else if (type.contains("Tag Name")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.tagName(xpath),text));
		} else if (type.contains("CSS Selector")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector(xpath),text));
		} else if (type.contains("Xpath")) {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(xpath),text));
		} else {
			elementToDisappear = wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(xpath),text));
		}
		 
		if (elementToDisappear) {
			element = findElement(driver,xpath,type);
		} else {
			throw new Exception("WebElement to be selected is not found. Xpath: " + xpath);
		}
		
		return elementToDisappear;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: presenceOfElementLocated
	 	Description: An expectation for checking that an element is present on the DOM of a page. This does not necessarily mean that the element is visible.
	 	Modifier: Public Method
	  	Input Parameters:
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				xpath - path of the element
				type - type of the path
		Output Parameters:
				webElement - the element to be used in this method
	*/
	/*----------------------------------------------------*/
	public WebElement presenceOfElementLocated(WebDriverWait wait, String xpath, String type) throws Exception {
		WebElement element = null;
		
		if (type.contains("ID")) {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id(xpath)));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				element = wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText(xpath)));
			} else {	
				element = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(xpath)));
			}
		} else if (type.contains("Name")) {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.name(xpath)));
		} else if (type.contains("Class Name")) {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.className(xpath)));
		} else if (type.contains("Tag Name")) {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.tagName(xpath)));
		} else if (type.contains("CSS Selector")) {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(xpath)));
		} else if (type.contains("Xpath")) {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		} else {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		}
		 
		return element;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: presenceOfElementLocated
	 	Description: An expectation for checking that an element with text is present on the DOM of a page. This does not necessarily mean that the element is visible.
	 	Modifier: Public Method
	  	Input Parameters:
	  		  	webDriver - the webDriver that controls the browser
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				xpath - path of the element
				type - type of the path
				text - the text to be evaluated
		Output Parameters:
				webElement - the element to be used in this method
	*/
	/*----------------------------------------------------*/
	public WebElement textToBePresentInElementLocated(WebDriver driver, WebDriverWait wait, String xpath, String type, String text) throws Exception {
		Boolean elementToAppear = false;
		WebElement element = null;
		
		if (type.contains("ID")) {
			elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.id(xpath),text));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.partialLinkText(xpath),text));
			} else {	
				elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.linkText(xpath),text));
			}
		} else if (type.contains("Name")) {
			elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.name(xpath),text));
		} else if (type.contains("Class Name")) {
			elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.className(xpath),text));
		} else if (type.contains("Tag Name")) {
			elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.tagName(xpath),text));
		} else if (type.contains("CSS Selector")) {
			elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.cssSelector(xpath),text));
		} else if (type.contains("Xpath")) {
			elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.xpath(xpath),text));
		} else {
			elementToAppear = wait.until(ExpectedConditions.textToBePresentInElementValue(By.xpath(xpath),text));
		}
		 
		if (elementToAppear) {
			element = findElement(driver,xpath,type);
		} else {
			throw new Exception("WebElement to be located with this text" + "\"" + text + "\" is not found. " + "Xpath: " + xpath);
		}

		return element;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: titleIs
	 	Description: An expectation for checking the title of a page.
	 	Modifier: Public Method
	  	Input Parameters:
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				text - the text to be evaluated
		Output Parameters:
				boolean - returns true if the element has appeared
	*/
	/*----------------------------------------------------*/
	public boolean titleIs(WebDriverWait wait, String text) throws Exception {
		Boolean textToAppear = wait.until(ExpectedConditions.titleIs(text));
		if (textToAppear) {
			LOGGER.info("Title is found: " + text);
		} else {
			throw new Exception("WebElement to be located with this text" + "\"" + text + "\" is not found.");
		}	
		return textToAppear;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: titleContains
	 	Description: An expectation for checking that the title contains a case-sensitive substring.
	 	Modifier: Public Method
	  	Input Parameters:
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				text - the text to be evaluated
		Output Parameters:
				boolean - returns true if the element has appeared
	*/
	/*----------------------------------------------------*/
	public boolean titleContains(WebDriverWait wait, String text) throws Exception {
		Boolean textToAppear = wait.until(ExpectedConditions.titleContains(text));
		if (textToAppear) {
			LOGGER.info("Title is found: " + text);
		} else {
			throw new Exception("WebElement to be located that contains this text" + "\"" + text + "\" is not found.");
		}	
		return textToAppear;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: visibilityOfElementLocated
	 	Description: An expectation for checking that an element is present on the DOM of a page and visible. Visibility means that the element is not only displayed but also has a height and width that is greater than 0.
	 	Modifier: Public Method
	  	Input Parameters:
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				xpath - path of the element
				type - type of the path
		Output Parameters:
				webElement - the element to be used in this method
	*/
	/*----------------------------------------------------*/	
	public WebElement visibilityOfElementLocated(WebDriverWait wait, String xpath, String type) throws Exception {
		WebElement element = null;
		
		if (type.contains("ID")) {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(xpath)));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(xpath)));
			} else {	
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(xpath)));
			}
		} else if (type.contains("Name")) {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(xpath)));
		} else if (type.contains("Class Name")) {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(xpath)));
		} else if (type.contains("Tag Name")) {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(xpath)));
		} else if (type.contains("CSS Selector")) {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(xpath)));
		} else if (type.contains("Xpath")) {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		} else {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		}
		
		return element;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: frameToBeAvailableAndSwitchToIt
	 	Description: If a page contains an Iframe which is an embedded html page in a webpage, you can use this to locate it
	 	Modifier: Public Method
	  	Input Parameters:
	  			WebDriverWait - the amount of wait time for the DOM to load before timing out
				xpath - path of the element
				type - type of the path
		Output Parameters:
				webDriver - the webDriver that controls the browser
	*/
	/*----------------------------------------------------*/	
	public WebDriver frameToBeAvailableAndSwitchToIt(WebDriverWait wait, String xpath, String type) throws Exception {
		WebDriver driver = null;
		
		if (type.contains("ID")) {
			driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id(xpath)));
		} else if (type.contains("Link Text")) {
			if (type.contains("Partial Link Text")) {
				driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.partialLinkText(xpath)));
			} else {	
				driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.linkText(xpath)));
			}
		} else if (type.contains("Name")) {
			driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.name(xpath)));
		} else if (type.contains("Class Name")) {
			driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.className(xpath)));
		} else if (type.contains("Tag Name")) {
			driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.tagName(xpath)));
		} else if (type.contains("CSS Selector")) {
			driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.cssSelector(xpath)));
		} else if (type.contains("Xpath")) {
			driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(xpath)));
		} else {
			driver = wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(xpath)));
		}
		
		return driver;
	}
	/*----------------------------------------------------*/
	/*	
	 	Method name: switchToParentFrame
	 	Description: This is the top context of the website. Commonly found on headers.
	 	Modifier: Public Method
	  	Input Parameters:
	  			webDriver - the webDriver that controls the browser
		Output Parameters:
				void
	*/
	/*----------------------------------------------------*/		
	public void switchToParentFrame(WebDriver driver) throws Exception {
		driver.switchTo().parentFrame();
	}
}
