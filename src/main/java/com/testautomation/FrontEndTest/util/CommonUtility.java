/*----------------------------------------------------*/
/*	
 	Class name: CommonUtility
 	Description: This class contains common utilities
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.util;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import com.teradata.FrontEndTest.main.*;
import com.testautomation.FrontEndTest.main.WebApplicationFrontEndTestKit;

public class CommonUtility {
    
	private static final Logger LOGGER = WebApplicationFrontEndTestKit.LOGGER;
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: logFileCreation
	 	Description: Variables are initialized in this method
	 	Modifier: Public Method
	  	Input Parameters:
	  			Logger logger -  the log handler of the class
	  			String filePath - the path where the logs will be created
	  			String logFileName - the name of the log file
	  			debugLevel - amount of debug messages 
		Output Parameters:
				PrintWriter - the utility that creates log messages in the log file
	*/
	/*----------------------------------------------------*/
    public static PrintWriter logFileCreation(Logger logger, String filePath, String logFileName, int debugLevel) throws Exception{
        
        FileHandler fileHandler;
        File checkFile = null;
        String filePathName = filePath;
        String errorMessage = "";
        PrintWriter writer = null;
        
        String dateText = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()); 
        String year = new SimpleDateFormat("yyyy").format(new Date());
        String month = new SimpleDateFormat("MM").format(new Date());
        String day = new SimpleDateFormat("yyyyMMdd").format(new Date());
        
        switch (Integer.valueOf(month)){
            case 1: month = month + "-January";
            break;
            case 2: month = month + "-February";
            break;
            case 3: month = month + "-March";
            break;
            case 4: month = month + "-April";
            break;
            case 5: month = month + "-May";
            break;
            case 6: month = month + "-June";
            break;
            case 7: month = month + "-July";
            break;
            case 8: month = month + "-August";
            break;
            case 9: month = month + "-September";
            break;
            case 10: month = month + "-October";
            break;
            case 11: month = month + "-November";
            break;
            case 12: month = month + "-December";
            break;
        }
        try {// if directoryPath is not available we create a directories to reflect it
            
            String directoryPath = filePath +  "\\" + year + "\\" + month + "\\" + day + "\\";
            Path path = Paths.get(directoryPath);
            
            if (!Files.exists(path)) {
                Files.createDirectories(path);
                if (debugLevel > 1){
                    LOGGER.info("Log Directory Created: " + path.toString());
                }
            }
            filePathName = directoryPath + dateText + "_" + logFileName +  ".log";
            
            //with the created directory above, we now create a log file
            //this uses the name defined in the parameters
            
            checkFile = new File(filePathName);
            if (!(checkFile.exists())) {

                writer = new PrintWriter(filePathName, "UTF-8");

                fileHandler = new FileHandler(filePathName);
                logger.addHandler(fileHandler);

                SimpleFormatter formatter = new SimpleFormatter();

                fileHandler.setFormatter(formatter);
            }
        } catch (NullPointerException ex) {   
            errorMessage = "File Path does not exist: " + filePathName;
            LOGGER.severe(errorMessage);
            throw new Exception(errorMessage);
        }
        if (debugLevel > 0){
            LOGGER.info("Log File Created: " + filePathName);
        }
        return writer;
    }
    
    public static boolean fileExists(String path) throws Exception {
    	File file = new File(path);
    	return file.exists();
    }
}
