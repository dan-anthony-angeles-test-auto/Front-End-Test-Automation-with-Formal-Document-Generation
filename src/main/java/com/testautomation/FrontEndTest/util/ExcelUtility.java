/*----------------------------------------------------*/
/*	
 	Class name: ExcelUtility
 	Description: This class contains utilities for Excel Related Operations
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;


public class ExcelUtility {

	/*----------------------------------------------------*/
	/*	
	 	Method name: getWorkBookName
	 	Description: returns the workbook name
	 	Modifier: Public Method
	  	Input Parameters: 
	  			String path - path of the excel file to be evaluated
		Output Parameters:
		 		String - workbook name 
	*/
	/*----------------------------------------------------*/
	public String getWorkBookName(String path) throws Exception {
		File file = new File(path);
		return file.getName();
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: setFiletoInputStream
	 	Description: sets the file to be evaluated to FileInputStream
	 	Modifier: Public Method
	  	Input Parameters: 
	  			String path - path of the excel file to be evaluated
		Output Parameters:
		 		FileInputStream - the file to be updated is in memory 
	*/
	/*----------------------------------------------------*/
	public FileInputStream setFiletoInputStream(String path) throws Exception {
	     File file = new File(path);
	     FileInputStream fileInput = new FileInputStream(file);
	     return fileInput;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: openWorkbook
	 	Description: opens the workbook 
	 	Modifier: Public Method
	  	Input Parameters: 
	  			FileInputStream - the file to be updated is in memory
		Output Parameters:
		 		XSSFWorkbook - workbook instance 
	*/
	/*----------------------------------------------------*/
	public XSSFWorkbook openWorkbook(FileInputStream fileInput) throws Exception {
	      XSSFWorkbook workbook = new XSSFWorkbook(fileInput);
	      return workbook;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: closeInputFileStream
	 	Description: closes the Input FileStream
	 	Modifier: Public Method
	  	Input Parameters: 
	  			FileInputStream - the file to be updated is in memory
		Output Parameters:
		 		void 
	*/
	/*----------------------------------------------------*/
	public void closeInputFileStream(FileInputStream fileInput) throws Exception {
		fileInput.close(); 
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: closeWorkbook
	 	Description: closes the workbook
	 	Modifier: Public Method
	  	Input Parameters: 
	  			XSSFWorkbook - workbook instance
	  			String path - path of the excel file to be evaluated
		Output Parameters:
		 		void 
	*/
	/*----------------------------------------------------*/
	public void closeWorkbook(XSSFWorkbook workbook, String path) throws Exception {
	    FileOutputStream outFile = new FileOutputStream(new File(path));
	    workbook.write(outFile);
	    outFile.close();
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: selectSheet
	 	Description: sets the sheet as active sheet
	 	Modifier: Public Method
	  	Input Parameters: 
	  			XSSFWorkbook - workbook instance
	  			String sheetName - name of the sheet to be activated
		Output Parameters:
		 		XSSFSheet - worksheet instance 
	*/
	/*----------------------------------------------------*/
	public XSSFSheet selectSheet(XSSFWorkbook workbook, String sheetName) throws Exception {
		XSSFSheet sheet = workbook.getSheet(sheetName);
		return sheet;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: setCellValue
	 	Description: sets the value of a cell
	 	Modifier: Public Method
	  	Input Parameters: 
	  			XSSFSheet - worksheet instance
	  			int row - row of the value to be set
	  			int col - column of the value to be set
	  			String value - the value to be set
		Output Parameters:
		 		void
	*/
	/*----------------------------------------------------*/
	public void setCellValue(XSSFSheet sheet, int row, int col, String value) throws Exception {
		XSSFCell cell = null;
		try {
	
			if(sheet.getRow(row) == null) {
				sheet.createRow(row);
			}
			if (sheet.getRow(row).getCell(col) == null) {
				cell = sheet.getRow(row).createCell(col);
			} else {
				cell = sheet.getRow(row).getCell(col);
			}
			
		} catch (NullPointerException e) {
			throw new Exception(e.getMessage());
		} finally {
			cell.setCellValue(value);	
		}
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: getCellValue
	 	Description: gets the value of a cell
	 	Modifier: Public Method
	  	Input Parameters: 
	  			XSSFSheet - worksheet instance
	  			int row - row of the value to be set
	  			int col - column of the value to be set
		Output Parameters:
		 		String value - retrieved value from the cell
	*/
	/*----------------------------------------------------*/
	public String getCellValue(XSSFSheet sheet, int row, int col) throws Exception {
		String value = "";
		try {
			if (sheet.getRow(row).getCell(col).toString() == null) {
				value = "";
			} else {
				value = sheet.getRow(row).getCell(col).toString();
			}
		} catch (NullPointerException e) {
			value = "";
		}
		return value;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: numberOfRows
	 	Description: returns the number of rows in a sheet
	 	Modifier: Public Method
	  	Input Parameters: 
	  			XSSFSheet - worksheet instance
		Output Parameters:
		 		int - returns the total rows of that sheet
	*/
	/*----------------------------------------------------*/
	public int numberOfRows(XSSFSheet sheet) {
		return sheet.getLastRowNum() + 1;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: numberOfRows
	 	Description: returns the number of rows in a sheet
	 	Modifier: Public Method
	  	Input Parameters: 
	  			XSSFSheet - worksheet instance
		Output Parameters:
		 		int - returns the total rows of that sheet
	*/
	/*----------------------------------------------------*/
	public int numberOfColumns(XSSFSheet sheet, int rowNum) {
		int maxColumn = 0;
		for (int i = 0;i < rowNum; i++) {
			if(Integer.valueOf(sheet.getRow(i).getLastCellNum()) > maxColumn) {
				maxColumn = Integer.valueOf(sheet.getRow(i).getLastCellNum());
			};
		};
		return maxColumn;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: sheetExists
	 	Description: returns a list of sheets that do not exist in the workbook
	 	Modifier: Public Method
	  	Input Parameters: 
	  			XSSFWorkbook - workbook instance
	  			List<String> sheetNames - list of sheet names to be evaluated
		Output Parameters:
		 		List<String> - list of sheets that are missing 
	*/
	/*----------------------------------------------------*/
	public List<String> sheetExists(XSSFWorkbook workbook, List<String> sheetNames) throws Exception {
		List<String> missingSheetNames = new ArrayList<String>();
		boolean sheetFound = false;
		
		for (int i = 0; i < sheetNames.size(); i++) {
		    if (workbook.getNumberOfSheets() > 0) {
		        for (int j = 0; j < workbook.getNumberOfSheets(); j++) {
		           if (workbook.getSheetName(j).equals(sheetNames.get(i))) {
		        	   sheetFound = true;
		           }
		        }
		    } else {
		    	throw new Exception("This workbook is empty.");
		    }
		    if (sheetFound ==  true) {
		    	sheetFound = false;
		    } else {
		    	missingSheetNames.add(sheetNames.get(i));
		    }
		}
		return missingSheetNames;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: clearSheet
	 	Description: clears contents of a sheet
	 	Modifier: Public Method
	  	Input Parameters: 
				XSSFSheet - worksheet instance
		Output Parameters:
		 		void 
	*/
	/*----------------------------------------------------*/
	public void clearSheet(XSSFSheet sheet) throws Exception {
		int rowNum = numberOfRows(sheet);
		System.out.print(rowNum);
		int colNum = numberOfColumns(sheet, rowNum);
		System.out.print(colNum);
		for (int i = 0; i < rowNum; i++) {
			for (int j = 0; j < colNum; j ++) {
				setCellValue(sheet,i,j,"");
			}
		}
		
		//for (Row row : sheet) {
	//		sheet.removeRow(row);
	//	}
	}
	
}
