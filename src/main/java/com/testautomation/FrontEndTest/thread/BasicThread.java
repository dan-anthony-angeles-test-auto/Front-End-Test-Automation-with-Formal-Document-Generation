/*----------------------------------------------------*/
/*	
 	Class name: BasicThread
 	Description: This class contains the basic thread to be used as basic flow 
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.thread;

public abstract class BasicThread {
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: initialize
	 	Description: Variables are initialized in this method
	 	Modifier: Public Method
	  	Input Parameters: none
		Output Parameters: void
	*/
	/*----------------------------------------------------*/
	public void initialize() throws Exception {
		//Input Details here for initialization
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: setup
	 	Description: Required Environment or code setup will be indicated in this method
	 	Modifier: Public Method
	  	Input Parameters: none
		Output Parameters: void
	*/
	/*----------------------------------------------------*/
	public void setup() throws Exception {
		//Input Details here for Setup
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: execute
	 	Description: This is where the actual operation is performed
	 	Modifier: Public Method
	  	Input Parameters: none
		Output Parameters: void
	*/
	/*----------------------------------------------------*/
	public void execute() throws Exception {
		//Input Details here for execution
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: exit
	 	Description: This is where the operation closes
	 	Modifier: Public Method
	  	Input Parameters: none
		Output Parameters: void
	*/
	/*----------------------------------------------------*/
	public void exit() throws Exception {
		//Input Details here to exit the code
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: sequence
	 	Description: This is where all methods above are being executed
	 	Modifier: Public Method
	  	Input Parameters: none
		Output Parameters: void
	*/
	/*----------------------------------------------------*/
	public void sequence() throws Exception {
		initialize();
		setup();
		execute();
		exit();
	}
	
}
