/*----------------------------------------------------*/
/*	
 	Class name: SeleniumFields
 	Description: This class contains fields for Selenium 
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.fields;

public class SeleniumFields {
	
	private String webDriverFile;
	private String url;
	
	public SeleniumFields(String webDriverFile, String url) {
		String temp = webDriverFile;
        //ensure that the filepath is readable
        temp = temp.replace("\\", "\\\\"); //if Windows filepath
		this.webDriverFile = temp;
		this.url = url;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: getWebDriverFile
	 	Description: returns string path of the web driver
	 	Modifier: Public Method
	  	Input Parameters: 
	  			None
		Output Parameters:
		 		String - returns string path of the web driver 
	*/
	/*----------------------------------------------------*/
	public String getWebDriverFile() {
		return this.webDriverFile;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: getURL
	 	Description: returns string path of the URL
	 	Modifier: Public Method
	  	Input Parameters: 
	  			None
		Output Parameters:
		 		String - returns string path of the URL 
	*/
	/*----------------------------------------------------*/
	public String getUrl() {
		return this.url;
	}
	
}
