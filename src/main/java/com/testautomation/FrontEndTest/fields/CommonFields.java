/*----------------------------------------------------*/
/*	
 	Class name: CommonFields
 	Description: This class contains fields that are common to all. 
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.fields;

import java.util.Map;

public class CommonFields {
	
	private int debugMessagesLevel;
	private boolean logFileCreation;
	private String logFileName;
	private String logFilePath;
	
	public CommonFields(int debugMessagesLevel, boolean logFileCreation, String logFileName, String logFilePath) {
		String temp = logFilePath;
		this.debugMessagesLevel = debugMessagesLevel;
		this.logFileCreation = logFileCreation;
		this.logFileName = logFileName;
        
        //ensure that the filepath is readable
        temp = temp.replace("\\", "\\\\"); //if Windows filepath
        if (!temp.substring(temp.length() - 1).equals("\\")){
            temp = temp + "\\";
        }
		this.logFilePath = temp;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: getDebugMsgLvl
	 	Description: returns integer value of debug level
	 	Modifier: Public Method
	  	Input Parameters: 
	  			None
		Output Parameters:
		 		int - returns integer value of debug level 
	*/
	/*----------------------------------------------------*/
	public int getDebugMsgLvl() {
		return this.debugMessagesLevel;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: getLogFileCreation
	 	Description: returns boolean value of the log file creation
	 	Modifier: Public Method
	  	Input Parameters: 
	  			None
		Output Parameters:
		 		int - returns integer value of debug level 
	*/
	/*----------------------------------------------------*/
	public boolean getLogFileCreation() {
		return this.logFileCreation;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: getLogFileName
	 	Description: returns String value of the Log File Name
	 	Modifier: Public Method
	  	Input Parameters: 
	  			None
		Output Parameters:
		 		String - returns String value of the Log File Name 
	*/
	/*----------------------------------------------------*/
	public String getLogFileName() {
		return this.logFileName;
	}

	/*----------------------------------------------------*/
	/*	
	 	Method name: getLogFilePath
	 	Description: returns String value of the Log File Path
	 	Modifier: Public Method
	  	Input Parameters: 
	  			None
		Output Parameters:
		 		String - returns String value of the Log File Path 
	*/
	/*----------------------------------------------------*/
	public String getLogFilePath() {
		return this.logFilePath;
	}
	
}
