/*----------------------------------------------------*/
/*	
 	Class name: ExcelFields
 	Description: This class contains fields for Excel 
 	Creation Date: 07/01/2020
	Modified by----------Date----------Details
	Dan Angeles			 07/01/2020	   Creation		  
*/
/*----------------------------------------------------*/
package com.testautomation.FrontEndTest.fields;

public class ExcelFields {
	
	private String excelFile;
	
	public ExcelFields(String excelFile) {
		
		String temp = excelFile;
        //ensure that the filepath is readable
		temp = temp.replace("\\", "\\\\"); //if Windows filepath
		this.excelFile = temp;
	}
	
	/*----------------------------------------------------*/
	/*	
	 	Method name: getExcelFile
	 	Description: returns String path of Excel File
	 	Modifier: Public Method
	  	Input Parameters: 
	  			None
		Output Parameters:
		 		String - returns String path of Excel File 
	*/
	/*----------------------------------------------------*/
	public String getExcelFile() {
		return this.excelFile;
	}
}